import { Container, makeStyles, TextField, Typography } from '@material-ui/core'
import React from 'react'
import { useState } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete';
const useStyles = makeStyles({
    field: {
        marginTop: 20,
        marginBottom: 20,
        display: 'block'
    }
});
const userRole=[
    { user_role: 'Admin', id: 1 },
    { user_role: 'Doctor', id: 2 },
    { user_role: 'Parmacist', id: 3 },
    { user_role: 'Receptionist', id: 4 },
]
 function Login() {

    const classes = useStyles()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [usernameError, setUsernameError] = useState(false)
    const [passwordError, setPasswordError] = useState(false)

    return (
        <Container>
            <Typography
                variant="h6"
                color="textSecondary"
                component="h2"
                gutterBottom
            >
                Login 
            </Typography>
            <form noValidate autoComplete="off">
                <TextField

                    className={classes.field}
                    label="username"
                    variant="outline"
                    fullWidth
                    required

                />
                <TextField
                    className={classes.field}
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outline"
                    fullWidth
                    required
                />
                <useAutocomplete
                    id="user-role"
                    options={userRole}
                    getOptionLabel={(userRole) => userRole.user_role}
                    style={{ width: 300 }}
                    renderInput={(params) =>
                        <TextField
                            {...params}
                            label="Select User Role"
                            variant="outlined"
                        />}
                />

            </form>
        </Container>
    )
}
export default Login;
